              synfig -- vector animation renderer

synfig is a vector based 2D animation package. It is designed to be
capable of producing feature-film quality animation. It eliminates the
need for tweening, preventing the need to hand-draw each frame. synfig
features spatial and temporal resolution independence (sharp and smooth
at any resolution or framerate), high dynamic range images, and a
flexible plugin system.

This package contains the renderer used to convert synfig .sif files to
raster images, videos and other formats. Layer types include geometric,
gradient, filter, distortion, transformation, fractal and others. Output
targets include JPEG, PNG, GIF, BMP, PPM, DV, OpenEXR, ffmpeg (MPEG1),
libavcodec (AVI), imagemagick (MIFF), yuv420p, MNG and others. 

Links

Web:  http://synfig.org/
SVN:  http://svn.voria.com/code/synfig/
Proj: http://sf.net/projects/synfig/
IRC:  irc://irc.freenode.net/synfig

Please use the IRC channel and the sf.net tracker to get support and
report bugs, request features and submit patches.

Copyright

Copyright 2002 Robert B. Quattlebaum Jr.
Copyright 2002 Adrian Bentley
Copyright 2006 Paul Wise
Copyright 2007 Chris Moore

Licence

This package is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
or visit http://www.gnu.org/licenses/gpl.html
