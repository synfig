#! /bin/sh
#
# Sinfg Bootstrap Script
# $Id: bootstrap,v 1.2 2005/01/10 07:40:26 darco Exp $
# 
# This script creates the configure script and Makefile.in files,
# and also fixes a few things in both to ensure a smooth build
# on all compilers and platforms.
#

# Grab the current directory and move to our own
CURR_DIR="$(pwd)"
cd "$(dirname $0)"

# Environment Variables
BOOTSTRAP_NAME="$(basename $0)"
CONFIG_DIR="$(pwd)/config"

[ -x /bin/cygpath ] && CONFIG_DIR="`cygpath -m -s "$CONFIG_DIR"`"
[ -x /bin/cygpath ] && CONFIG_DIR="`cygpath -u "$CONFIG_DIR"`"
[ -x /bin/cygpath ] && CURR_DIR="`cygpath -m -s "$CURR_DIR"`"
[ -x /bin/cygpath ] && CURR_DIR="`cygpath -u "$CURR_DIR"`"
# Automake,Autoconf, and Libtool versions that we require
#AUTOCONF_VERSION=2.5
#AUTOMAKE_VERSION=1.6
#LIBTOOL_VERSION=1.4
export WANT_AUTOMAKE=1.8
#export WANT_LIBTOOL_VER=1.5
export WANT_AUTOCONF_2_5=1
export WANT_AUTOCONF=2.5

. $CONFIG_DIR/build.cfg

SED_SCRIPT="
s/@PACKAGE@/$PACKAGE/g;
s/@PACKAGE_NAME@/$PACKAGE_NAME/g;
s/@PACKAGE_BUGREPORT@/$PACKAGE_BUGREPORT/g;
s/@PACKAGE_TARNAME@/$PACKAGE_TARNAME/g;
s/@PACKAGE_VERSION@/$PACKAGE_VERSION/g;
s|@SVN_REPOSITORY@|$SVN_REPOSITORY|g;
s/@VERSION@/$VERSION/g;
s/@API_VERSION@/$API_VERSION/g;
s/@VERSION_MAJ@/$VERSION_MAJ/g;
s/@VERSION_MIN@/$VERSION_MIN/g;
s/@VERSION_REV@/$VERSION_REV/g;
s/@VERSION_REL@/$VERSION_REL/g;
s/@CFLAGS@//g;
"

# Define the output function
output () {
	echo $BOOTSTRAP_NAME: $*
}

# Define the cleanup function
cleanup () {
	output Cleaning up...
	rm -fr config.cache autom4te*.cache configure.in $TEMPFILE
}

output Prepairing build environment for $PACKAGE-$VERSION...

# Look for the CVS directory. If we don't find it, we need to
# ask the user if they know what they are doing.
( test -d CVS || test -d .svn ) ||
{
	output warning: This shell script is intended for those
	output warning: who either know what they are doing or
	output warning: or downloaded this program from the CVS
	output warning: repository. See README for more details.
	output warning: To avoid seeing this message in the future,
	output warning: create an empty directory called 'CVS'.
	output Waiting for 15 seconds...
	sleep 15
}

# Create the temporary file
output Creating temporary file...
TEMPFILE=`mktemp /tmp/$BOOTSTRAP_NAME.XXXXXX` ||
{
	output ERROR: Unable to create temporary file!
	exit 1
}

# Check for autoconf
(which autoconf > /dev/null 2>&1 ) ||
{
	output error: 'Could not find GNU autoconf!'
	output You need to download and install GNU autoconf v2.52 or higher.
	output '<ftp://ftp.gnu.org/gnu/autoconf/>'
	cleanup;
	exit 1
}

# Check autoconf version
output Using $(autoconf --version | grep utoconf)
autoconf --version | grep -q "$AUTOCONF_VERSION" ||
{
	output warning: Unexpected version of GNU Autoconf "(expected $AUTOCONF_VERSION)"
	output warning: *** Bootstrap process may fail!
}

# Check for automake
(which automake > /dev/null 2>&1 ) ||
{
	output error: 'Could not find GNU automake!'
	output You need to download and install GNU automake v1.5 or higher.
	output '<ftp://ftp.gnu.org/gnu/automake/>'
	cleanup;
	exit 1
}

# Check automake version
output Using $(automake --version | grep utomake)
automake --version | grep -q "$AUTOMAKE_VERSION" ||
{
	output warning: Unexpected version of GNU Automake "(expected $AUTOMAKE_VERSION)"
	output warning: *** Bootstrap process may fail!
}

# Check for libtool
(which libtool > /dev/null 2>&1 ) ||
{
	output error: 'Could not find GNU libtool!'
	output You need to download and install GNU libtool v1.4 or higher.
	output '<ftp://ftp.gnu.org/gnu/libtool/>'
	exit 1
}

# Check libtool version
echo $BOOTSTRAP_NAME: Using $(libtoolize --version | grep ibtool)
libtoolize --version | grep -q "$LIBTOOL_VERSION" ||
{
	output warning: Unexpected version of GNU Libtool "(expected $LIBTOOL_VERSION)"
	output warning: *** Bootstrap process may fail!
}

# Versions of libtool prior to 1.4.2 have a seriously broken libltdl.
# If we are using this broken version, we need to patch it.
libtoolize --version | grep -q -e "1.4.2" -e "1.4.1" -e "1.4 " && PATCH_LTDL=1

RECONFIG_LTDL=1

for FILENAME in doxygen.cfg pkgconfig.pc project.spec ; do {
output Creating $FILENAME...
sed "$SED_SCRIPT" < $CONFIG_DIR/$FILENAME.in > $FILENAME;
} ; done

output Renaming pkgconfig.pc to $PACKAGE_TARNAME.pc.in...
mv pkgconfig.pc "$PACKAGE_TARNAME.pc.in"

output Renaming project.spec to $PACKAGE-$VERSION.spec...
mv project.spec "$PACKAGE-$VERSION.spec"

output Finishing up $PACKAGE-$VERSION.spec...
echo %changelog >> "$PACKAGE-$VERSION.spec"
cat NEWS >> "$PACKAGE-$VERSION.spec"

output Creating configure.in from configure.ac...
sed "$SED_SCRIPT" < $CONFIG_DIR/configure.ac > configure.in;

output Setting up build environment...

# Set the shell to output what we are doing
set -x

# Create all of the build environment files
(
	libtoolize -c -f --ltdl &&
	aclocal -I $CONFIG_DIR $ACLOCAL_FLAGS &&
	autoheader &&
	autoconf -o configure &&
	automake --foreign --add-missing --copy --include-deps &&
	true
) ||
{
	# Something went wrong...
	set +x
	output Failure.
	cleanup;
	exit 1
}

# Turn off echoing of commands
set +x

# Reconfigure the LTDL, if necessary
[ $RECONFIG_LTDL"x" != "x" ] && (
	output Reconfiguring LTDL...
	set -x
	(
		cd libltdl &&
		( echo "AC_CONFIG_AUX_DIR(../config)" >> configure.[ai][cn] ) &&
		aclocal -I $CONFIG_DIR &&
		autoheader &&
		autoconf &&
		automake --foreign --add-missing --copy
	) ||
	{
		# Something went wrong...
		set +x
		output Failure.
		cleanup;
		exit 1
	}
	set +x
)

# If this is the broken version of LTDL, then patch it
[ $PATCH_LTDL"x" != "x" ] && (
	output Patching LTDL...
	sed "
		s/handle || /(handle \&\& handle->loader) || /;
		s/errors > 0) \&\& file_not_found ())/errors > 0) \&\& !file_not_found ())/;
		s/(!handle)/(!handle || !handle->loader)/;
		s/ access (filename, R_OK)/ !access (filename, R_OK)/;
	" < libltdl/ltdl.c > $TEMPFILE && cp $TEMPFILE libltdl/ltdl.c
	sed "
		s/DLL_EXPORT/LTDL_DLL_EXPORT/;
	" < libltdl/ltdl.h > $TEMPFILE && cp $TEMPFILE libltdl/ltdl.h
)

(
output Patching libtool...
patch config/ltmain.sh config/ltmain.patch
) || true
false && {
	# Something went wrong...
	set +x
	output Failure.
	cleanup;
	exit 1
}

#echo '#define LTDL_SHLIB_EXT	".so"' >> libltdl/config-h.in
#echo '
#ifdef WIN32
#define LTDL_SHLIB_EXT	".dll"
#else
#if __APPLE__
#define LTDL_SHLIB_EXT	".so"
#else
#define LTDL_SHLIB_EXT	".la"
#endif
#endif ' >> libltdl/config-h.in

# output Patching configure script to look for gcc3...
# sed "
# s/g++ c++/g++3 g++ c++/;
# s/gcc cc/gcc3 gcc cc/;
# s:"'${prefix}/include'":"'${prefix}/include/sinfg'":;
# s:PREFIX/include:PREFIX/include/ETL:;
# " < configure > $TEMPFILE
# cp $TEMPFILE configure

# Patch the Makefile.in files
for filename in $(find Makefile.in src -name Makefile.in) ; do {
	echo $BOOTSTRAP_NAME: Patching $filename
	(
		cp $filename $TEMPFILE &&
		sed "
			# Remove brain-dead include path
			s;-I. ;;
			
			# Gosh... I can't remember why I did this one...
			# Everything seems to work, so I'll leave it in.
			s;-I"'$(srcdir)'" ;-I"'$(top_srcdir)'" ;

			# Tell the configure script where it's origin realy is
			s;configure.in;config/configure.ac;
		" < $TEMPFILE > $filename
	) ||
	{
		# Patch failure
		output Failure. Unable to patch $filename.
		cleanup;
		exit 1
	}
}; done

output Creating Makefile...
( echo "
all:
	./configure --enable-maintainer-mode
	make all
	
install:
	./configure --enable-maintainer-mode
	make install
	
check:
	./configure --enable-maintainer-mode
	make check

distcheck:
	./configure --enable-maintainer-mode
	make check
	
dist:
	./configure --enable-maintainer-mode
	make dist

docs: doxygen.cfg
	doxygen doxygen.cfg

" ) > Makefile

output Complete.

cleanup;

# Move back to the current directory
cd $CURR_DIR

make -C src/synfig/proto
