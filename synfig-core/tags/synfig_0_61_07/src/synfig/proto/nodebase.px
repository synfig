/* === S Y N F I G ========================================================= */
/*!	\file nodebase.h
**	\brief Template Header
**
**	$Id$
**
**	\legal
**	Copyright (c) 2002-2005 Robert B. Quattlebaum Jr., Adrian Bentley
**
**	This package is free software; you can redistribute it and/or
**	modify it under the terms of the GNU General Public License as
**	published by the Free Software Foundation; either version 2 of
**	the License, or (at your option) any later version.
**
**	This package is distributed in the hope that it will be useful,
**	but WITHOUT ANY WARRANTY; without even the implied warranty of
**	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**	General Public License for more details.
**	\endlegal
*/
/* ========================================================================= */

/* === S T A R T =========================================================== */

#ifndef __SYNFIG_NODEBASE_H
#define __SYNFIG_NODEBASE_H

/* === H E A D E R S ======================================================= */

#include "../protocol.h"
#include "../string.h"
#include "../guid.h"
#include <sigc++/slot.h>

/* === M A C R O S ========================================================= */

/* === T Y P E D E F S ===================================================== */

/* === C L A S S E S & S T R U C T S ======================================= */

namespace synfig {
namespace Proto {

typedef int Query;
typedef int NodeList;

class NodeBase : public Protocol
{
public:

	PX_DEFINE_DATA(guid, GUID)

	PX_DEFINE_FUNC(func_test, float, int, int)

	PX_DEFINE_DATA(id, String)

	PX_DEFINE_DATA(root, NodeHandle)

	PX_DEFINE_FUNC(signal_changed, sigc::signal<void>)
	PX_DEFINE_FUNC(signal_deleted, sigc::signal<void>)
		
	PX_DEFINE_FUNC_CONST(get_parents, const NodeList)
	PX_DEFINE_FUNC_CONST(get_children, const NodeList)

	PX_DEFINE_FUNC(query_children, NodeList, Query)

}; // END of class Proto::NodeBase

}; // END of namespace Proto
}; // END of namespace synfig

/* === E N D =============================================================== */

#endif
