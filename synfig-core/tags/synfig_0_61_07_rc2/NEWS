              synfig releases

 0.61.07 (SVN ???) - ???? ??, 200? - Bug fixes

  * Fix importing on amd64 (#1692825)
  * Fixes for the PasteCanvas layer (#1356449)
  * Clean up Layer menu and displayed layer names
  * Allow exported canvases to be used in more than one place within a canvas (#1785296)
  * Fix, extend, and enable the 'XOR Pattern' layer
  * Fix and extend the 'Plant' layer (#1790903)
  * Fix and enable the 'Timed Swap' valuenode conversion
  * Extend the 'Linear' valuenode conversion to work with colors and integers.
  * Enable basic motion blur display at all qualities
  * Fix an artifact on the curve gradient layer (#1792063, r663)
  * Add the 'Range', 'Repeat Gradient', 'Add', 'Exponent', 'BLine Vector', and 'BLine Tangent' valuenode conversions (#1781903)
  * Fix various problems with valuenode conversion  (#1794374, #1795913, #1795922, #1796068, #1797488)
  * Allow gradients to be animated (#1568818, #1795802)
  * Stop TCB angle waypoints changing to type Linear on load (#1778930)
  * Use compression when saving to files with .sifz extension
  * Compressed example .sif files to .sifz to save space
  * Fixed Motion Blur layer (#1800783)
  * Allow building studio with gcc -O2 (#1509627)
  * Allow encapsulated layers to animate their z-depth, even when time-shifted (#1806852)
  * Fixed the Radial Blur layer.  It previously wasn't working when zooming in on the canvas (#1807709)
  * Fix several other bugs and crashes

 0.61.06 (SVN 536) - June 20, 2007 - Bug fixes

  * Add fontconfig support
  * Fix amd64 issue
  * Fix ffmpeg, gif, libav targets
  * Include more target modules in the Win32 package
  * Fix some crashes in synfigstudio
  * Fix some render artifacts
  * Fix some doxygen warnings
  * Some MacOS fixes
  * Misc bug fixes
  * Fix random number generation for 64 bit CPUs (#1698604)
  * Add parameter 'fast' to curve gradients allowing choice between fast or accurate rendering (#1672033)
  * Add new odd/even winding style for regions

 0.61.05 (SVN 126) - February 27, 2005 - Misc fixes

  * Use system libltdl when available (#1423944)
  * Update doxygen config file (#1402909)
  * Fix fontconfig/xft FTBFS
  * Misc fix (#1414370)

 0.61.04 (SVN 102) - January 10, 2005 - Misc fixes

  * Check for imagemagick at build time
  * Clarify mod_openexr copyright
  * Fix mod_openexr building
  * Don't add 'Copyright Voria' to PNG output
  * Don't write localised numbers when generating SIF files

 0.61.03 - December 8, 2005 - Copyright update

  * Update more old copyright and licence notices
  * Remove broken walk example
  * Misc code fixes

 0.61.02 - November 26, 2005 - Misc fixes

  * Small cygwin/optimization/code fixes

 0.61.01 - November 3, 2005 - Copyright update

  * Update old copyright and licence notices
  * Fix some GCC 4.0 build issues
  * Include errno.h where needed
  * Add ./configure options for debug, warnings, optimisation, profiling
  * Add about_dialog.sif as another example
  * Remove SFAutomaton.ttf due to licence issues
  * Fix BMP rendering target

 0.61.00-39 - November 1, 2005 - Developer preview

  * First public release!

