              synfigstudio -- vector animation studio

synfig is a vector based 2D animation package. It is designed to be
capable of producing feature-film quality animation. It eliminates the
need for tweening, preventing the need to hand-draw each frame. synfig
features spatial and temporal resolution independence (sharp and smooth
at any resolution or framerate), high dynamic range images, and a
flexible plugin system.

synfigstudio is the animation studio for synfig and provides the GUI
interface to create synfig animations which are saved in synfig .sif
format.

Links

Web:  http://synfig.org/
Code: http://synfig.org/Source_code
Proj: http://sf.net/projects/synfig/
IRC:  irc://irc.freenode.net/synfig

Please use the IRC channel and the sf.net tracker to get support and
report bugs, request features and submit patches.

Copyright

Copyright 2002 Robert B. Quattlebaum Jr.
Copyright 2002 Adrian Bentley
Copyright 2006 Yue Shi Lai
Copyright 2007-2008 Chris Moore
Copyright 2007-2008 Paul Wise
Copyright 2008 Aurore D.
Copyright 2008 Gerald Young
Copyright 2008 David Roden
Copyright 2008 Daniel Hornung
Copyright 2008 Carlos López
Copyright 2008 Gerco Ballintijn

Some of the icons are placed in the Public Domain by Chris Norman
Some of the icons are placed in the Public Domain by Carlos López González
Some of the artwork is placed in the Public Domain by Franco Iacomella and Carlos López González

Licence

This package is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
or visit http://www.gnu.org/licenses/gpl.html
