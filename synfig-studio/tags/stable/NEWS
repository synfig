              synfigstudio releases

More detailed notes are available on the releases page:

http://synfig.org/Releases

 0.61.09 (SVN 2114) - October 21, 2008 - Bug fixes, features
 
  * Fix building synfigstudio in the fink distribution
  * Improved plant layer
  * Added new options for Circle and Rectangle tools
  * Added Text and Star tools.
  * Added new plant, text, star and polygon icons
  * Added Spanish, French and Catalan translations
  * Added "Link to Bline" feature
  * Improved main window zoom behaviour and added new zoom icons
  * Several fixes and improvements on timetrack slider and waypoints drawing
  * Depreciate Manual as interpolation type
  * Added new icons for reset and swap FG and BG colors. Re-arrange them 
    to show properly in all GTK themes
  * Added some improvements in the Render Description
  * Improved GUI for the sound selection dialog
  * When adding layers leave the previously selected layer selected
  * Button tools are arranged in the Tool box
  * Added and fixed some keyboard shortcuts
  * Show only shared compatible parameters when more than one layer is selected
  * Added more functionality to colour and time sliders
  * Added new 'Document' tab in Setup dialog for more user preferences
  * Fixed some crashes and errors
  * Width tool returned to the Tool Box by default.

 0.61.08 (SVN 1839) - March 3, 2008 - Bug fixes, features

  * Update the website URL to synfig.org instead of synfig.com
  * Synfig is now translatable using gettext
  * Removed the 'bootstrap' script for building. Use autoreconf instead
  * Fixed errors and warnings detected by a pre-release version of GCC 4.3
  * Fixes to allow sigc++ 2.1 and newer to be used.
  * Add a new bog-standard about dialog and a new splash screen
  * Add a new icons for rename, encapsulate, show child layers and time
  * Usability enhancements for new users, env vars to control them
  * Improvements to the draw, bline and sketch tools
  * Improvements for linking, quality menu, save dialog, render dialog
  * Cut down on the console output and output a user-friendly message
  * Don't wrap angles at 360 degrees, allows better rotations
  * Settings for auto-backup interval, run everything in one thread
  * Fix waypoint issues; drawing, menus, crash, label and others
  * Fix time issues; time field expansion & editing, ticks and others
  * Fix ducks issues; prioritise & restrict radius, show circle size, others
  * Allow horizontal scrolling the time field and the default brush size
  * Allow different pixel sizes in the workarea window
  * Remember window locations and ignore corrupted values
  * Widen the toolbox to 5 icons instead of 4 icons
  * Tile renderer changes: disable with env var, fix wrong placing
  * Improves the details in the history dialog quite a bit
  * Fix typos, several crashes, usability and other issues

0.61.07 (SVN 878) - October 10, 2007 - Bug fixes

  * Fix some tooltips and menu stuff (#1787437)
  * Allow left-right scrolling in canvas window with shift + mouse-wheel
  * Rework mouse interaction with ducks and tangents
  * Move icons to a subdir
  * Fix some typos, several crashes (#1780016, #1785598) and other bugs
  * Re-enable sketch, draw, polygon by default
  * Add keyboard shortcuts for all tools ( http://synfig.org/Keyboard_Shortcuts )
  * Fix crash when setting canvas resolution to zero (#1779616)
  * Add 'auto-export' checkbox and layer name box to the draw tool
  * Various GUI fixes (#1794006, #1796110)
  * File-selector fixes (#1383736)
  * Fix the View>Play menu entry to allow playback inside the workarea, shortcut Ctrl-P
  * Save to .sifz (compressed) format by default if no extension is given
  * Remember the path used when saving a document and default to it in future
  * Call new documents "Synfig Animation <n>" by default, rather than "untitled0"
  * Don't quit until the user clearly answers "do you want to save?" (#1799722)
  * Remember the filename the animation was last rendered to, and default to it (#1799250)
  * Improved the 'Groups' function a little (#1368694).  It's still quite broken
  * Fixed the colors displayed on the sliders in the Color Editor dialog
  * Added option to Bline Tool to allow positions to be automatically linked (#1776156)
  * Added menu entry to restore all dialogs to their default positions
  * Fix red "Animate Editing Mode" frame so it shows up in all themes (#1801220)
  * Allow user to "export SYNFIG_DISABLE_POPUP_WINDOWS=1" to make splash screen etc. less obtrusive
  * Fixed the logic used to decide which value to use when linking values together
  * Update child canvas time sliders when parent time slider is moved
  * Prevent the caret menu entries from being disabled when another canvas is closed
  * Show the correct initial quality level in the View > Preview Quality menu

 0.61.06 (SVN 543) - June 20, 2007 - Bug fixes
 
  * Fix amd64 issues
  * Fix corruption of gamma settings
  * Fix manipulating layers in the layer dialog
  * Disable the timebar instead of hiding it
  * Fix File -> Close & input devices close button
  * Fix targets that crash in synfigstudio
  * Fix some doxygen warnings
  * Fixes in the sketch tool
  * Fixes in the draw tool
  * Fix several crashes
  * Fix bline looping
  * Misc Win32 fixes
  * Add optional GNOME thumbnailer
  * Rework several dialogs to use GNOME HIG
  * Disable several tools, see FAQ to re-enable
  * Disable the empty Palette Browser dialog
  * Add mousewheel support in canvas windows
  * Add tooltips to preview window & give it a default size
  * Display HTML values in the colour dialog
  * Add an icon for the curves dialog
  * New Tango ArtLibreSet styled icons by PXEGeek

 0.61.05 (SVN 128) - February 27, 2006 - build issues

  * Fixed building with gtkmm 2.8 (#1373933, #1358304)
  * Allow the toolbox to come to the front (#1367916)
  * Make the user prefs dir configurable at build time
  * Fixed minor build issues

 0.61.04 (SVN 103) - January 10, 2006 - MacOS X packaging

  * Fixes for the MacOS X packaging

 0.61.03 - December 8, 2005 - Build & copyright fixes

  * Update more old copyright and licence notices
  * Fully fix building with GCC 4

 0.61.02 - November 26, 2005 - Build fixes

  * Better Win32 / MacOS X packaging
  * Use new GTK+ file chooser
  * Adds some GCC 4 tweaks
  * Fixes crash on layer reorder
  * Include errno.h where needed
  * Other tweaks

 0.61.01 - November 6, 2005 - Copyright updates

  * Update old copyright and licence notices
  * Add ./configure options for debug, warnings, optimisation, profiling
  * Uses per-os directory separator
  * Add TODO file

 0.61.00-39 - November 1, 2005 - Developer preview

  * First public release!

