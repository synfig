/* === S Y N F I G ========================================================= */
/*!	\file dock_layers.cpp
**	\brief Template File
**
**	$Id: dock_layers.cpp,v 1.2 2005/01/12 07:03:42 darco Exp $
**
**	\legal
**	Copyright (c) 2002-2005 Robert B. Quattlebaum Jr., Adrian Bentley
**
**	This package is free software; you can redistribute it and/or
**	modify it under the terms of the GNU General Public License as
**	published by the Free Software Foundation; either version 2 of
**	the License, or (at your option) any later version.
**
**	This package is distributed in the hope that it will be useful,
**	but WITHOUT ANY WARRANTY; without even the implied warranty of
**	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**	General Public License for more details.
**	\endlegal
*/
/* ========================================================================= */

/* === H E A D E R S ======================================================= */

#ifdef USING_PCH
#	include "pch.h"
#else
#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif

#include "dock_layers.h"
#include "app.h"

#include <gtkmm/scrolledwindow.h>
#include <cassert>
#include "instance.h"
#include <sigc++/signal.h>
#include <sigc++/hide.h>
#include <sigc++/retype_return.h>
#include <sigc++/slot.h>
#include "layertreestore.h"
#include "layertree.h"
#include "canvasview.h"
#include "layeractionmanager.h"
//#include <ETL/ref_count>

#endif

/* === U S I N G =========================================================== */

using namespace std;
using namespace etl;
using namespace synfig;
using namespace studio;

/* === M A C R O S ========================================================= */

/* === G L O B A L S ======================================================= */

/* === P R O C E D U R E S ================================================= */

/*static void do_nothing(reference_counter x)
{
	synfig::info(__FILE__":%d:ref_count.count()=%d",__LINE__,x.count());
}*/

/* === M E T H O D S ======================================================= */

Dock_Layers::Dock_Layers():
	Dock_CanvasSpecific("layers",_("Layers"),Gtk::StockID("synfig-layer")),
//	layer_action_manager(0)
	layer_action_manager(new LayerActionManager)
{
	if(layer_action_manager)layer_action_manager->set_ui_manager(App::ui_manager());

	action_group_new_layers=Gtk::ActionGroup::create();
	action_group_layer_ops=Gtk::ActionGroup::create();
	
	std::map<synfig::String,synfig::String> category_map;

	// Build layer creation actions
	synfig::Layer::Book::iterator iter;
	for(iter=synfig::Layer::book().begin();iter!=synfig::Layer::book().end();++iter)
	{
		synfig::Layer::Book::value_type lyr(*iter);
		
		if(lyr.second.category==_("Do Not Use"))
			continue;
		
		action_group_new_layers->add(Gtk::Action::create(
			strprintf("layer-new-%s",lyr.first.c_str()),
			layer_icon(lyr.first.c_str()),
			lyr.second.local_name,lyr.second.local_name
		),
			sigc::hide_return(
				sigc::bind(
					sigc::mem_fun(*this,&studio::Dock_Layers::add_layer),
					lyr.first
				)
			)
		);

		category_map[lyr.second.category]+=strprintf("<menuitem action='layer-new-%s' />",lyr.first.c_str());
	
		//(*category_map)[lyr.second.category]->items().push_back(Gtk::Menu_Helpers::MenuElem(lyr.second.local_name,
		//));
	}
	
	{
		Glib::RefPtr<Gtk::ActionGroup> action_group_categories(Gtk::ActionGroup::create("layer-category"));
		synfig::String layer_ui_info;
		
		layer_ui_info+="<ui><menubar action='menu-main'><menu action='menu-layer'><menu action='menu-layer-new'>";

		std::map<synfig::String,synfig::String>::iterator iter;
		for(iter=category_map.begin();iter!=category_map.end();++iter)
		{
			layer_ui_info+=strprintf("<menu action='%s'>%s</menu>",iter->first.c_str(),iter->second.c_str());
			action_group_categories->add(Gtk::Action::create(iter->first.c_str(),iter->first.c_str()));
		}

		layer_ui_info+="</menu></menu></menubar></ui>";
		
		App::ui_manager()->insert_action_group(action_group_categories);
		App::ui_manager()->insert_action_group(action_group_new_layers);
		App::ui_manager()->add_ui_from_string(layer_ui_info);
	}
	
	
	action_group_layer_ops->add( Gtk::Action::create("toolbar-layer", "Layer Ops") );
	App::ui_manager()->insert_action_group(action_group_layer_ops);


    Glib::ustring ui_info =
	"<ui>"
	"	<toolbar action='toolbar-layer'>"
	"	<toolitem action='action-layer_raise' />"
	"	<toolitem action='action-layer_lower' />"
	"	<toolitem action='action-layer_duplicate' />"
	"	<toolitem action='action-layer_remove' />"
	"	<toolitem action='cut' />"
	"	<toolitem action='copy' />"
	"	<toolitem action='paste' />"
	"	</toolbar>"
	"</ui>"
	;

	App::ui_manager()->add_ui_from_string(ui_info);

	action_group_new_layers->set_sensitive(false);

	set_toolbar(*dynamic_cast<Gtk::Toolbar*>(App::ui_manager()->get_widget("/toolbar-layer")));	





	/*
	reference_counter ref_count;
	synfig::info(__FILE__":%d:ref_count.count()=%d",__LINE__,ref_count.count());

	{
		sigc::signal<void> tmp_signal;
		
		tmp_signal.connect(
			sigc::bind(
				sigc::ptr_fun(do_nothing),
				ref_count
			)
		);
		
	synfig::info(__FILE__":%d:ref_count.count()=%d",__LINE__,ref_count.count());
		tmp_signal();
	synfig::info(__FILE__":%d:ref_count.count()=%d",__LINE__,ref_count.count());
		
		tmp_signal.clear();
	synfig::info(__FILE__":%d:ref_count.count()=%d",__LINE__,ref_count.count());
		
		tmp_signal();
	synfig::info(__FILE__":%d:ref_count.count()=%d",__LINE__,ref_count.count());
		tmp_signal.connect(
			sigc::bind(
				sigc::ptr_fun(do_nothing),
				ref_count
			)
		);
	synfig::info(__FILE__":%d:ref_count.count()=%d",__LINE__,ref_count.count());
	}
	synfig::info(__FILE__":%d:ref_count.count()=%d",__LINE__,ref_count.count());
	assert(ref_count.count()==1);	
	*/
}


Dock_Layers::~Dock_Layers()
{
	delete layer_action_manager;
}


void
Dock_Layers::init_canvas_view_vfunc(etl::loose_handle<CanvasView> canvas_view)
{
	Glib::RefPtr<LayerTreeStore> layer_tree_store;
	layer_tree_store=LayerTreeStore::create(canvas_view->canvas_interface());

	canvas_view->set_tree_model(get_name(),layer_tree_store);
	LayerTree* layer_tree(new LayerTree());
	layer_tree->set_model(layer_tree_store);
	layer_tree->set_time_adjustment(canvas_view->time_adjustment());

	layer_tree->signal_edited_value().connect(
		sigc::hide_return(
			sigc::mem_fun(*canvas_view->canvas_interface(), &synfigapp::CanvasInterface::change_value)
		)
	);

	canvas_view->set_ext_widget(get_name()+"_cmp",layer_tree);
	canvas_view->set_ext_widget(get_name(),&layer_tree->get_layer_tree_view());
	canvas_view->set_ext_widget("params",&layer_tree->get_param_tree_view());
	canvas_view->set_tree_model("params",layer_tree->get_param_tree_view().get_model());

	/*
	canvas_view->layermenu.items().push_back(Gtk::Menu_Helpers::StockMenuElem(Gtk::StockID("gtk-delete"),Gtk::AccelKey("Delete"),
		sigc::mem_fun(*layer_tree, &LayerTree::on_delete_pressed))
	);
	*/

	// Hide the time bar
	if(canvas_view->get_canvas()->rend_desc().get_time_start()==canvas_view->get_canvas()->rend_desc().get_time_end())
		canvas_view->hide_timebar();
	layer_tree_store->rebuild();
	present();
}

void
Dock_Layers::changed_canvas_view_vfunc(etl::loose_handle<CanvasView> canvas_view)
{
	if(canvas_view)
	{
		Gtk::Widget* tree_view(canvas_view->get_ext_widget(get_name()));
	
		add(*tree_view);
		tree_view->show();
		action_group_new_layers->set_sensitive(true);
		if(layer_action_manager)
		{
			layer_action_manager->set_layer_tree(dynamic_cast<LayerTree*>(canvas_view->get_ext_widget(get_name()+"_cmp")));
			layer_action_manager->set_canvas_interface(canvas_view->canvas_interface());
			layer_action_manager->refresh();
		}
	}
	else
	{
		action_group_new_layers->set_sensitive(false);
		if(layer_action_manager)
		{
			layer_action_manager->clear();
			layer_action_manager->set_canvas_interface(0);
			layer_action_manager->set_layer_tree(0);
		}
		
		clear_previous();		
	}
}

void
Dock_Layers::add_layer(synfig::String id)
{
	etl::loose_handle<CanvasView> canvas_view(get_canvas_view());
	if(canvas_view)
	{
		canvas_view->add_layer(id);
	}
}
