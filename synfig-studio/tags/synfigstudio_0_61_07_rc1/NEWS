              synfigstudio releases

 0.61.07 (SVN ???) - ???? ??, 200? - Bug fixes

  * Fix some tooltips and menu stuff (#1787437)
  * Allow left-right scrolling in canvas window
  * Rework mouse interaction with ducks and tangents
  * Move icons to a subdir
  * Fix some typos, several crashes (#1780016, #1785598) and other bugs
  * Re-enable sketch, draw, polygon by default
  * Add keyboard shortcuts for all tools ( http://wiki.synfig.com/Keyboard_Shortcuts )
  * Fix crash when setting canvas resolution to zero (#1779616)
  * Add 'auto-export' checkbox and layer name box to the draw tool
  * Various GUI fixes (#1794006, #1796110)
  * File-selector fixes (#1383736)
  * Fix the View>Play menu entry to allow playback inside the workarea, shortcut Ctrl-P
  * Save to .sifz (compressed) format by default if no extension is given
  * Remember the path used when saving a document and default to it in future
  * Call new documents "Synfig Animation <n>" by default, rather than "untitled0"
  * Don't quit until the user clearly answers "do you want to save?" (#1799722)
  * Remember the filename the animation was last rendered to, and default to it (#1799250)
  * Improved the 'Groups' function a little (#1368694).  It's still quite broken
  * Fixed the colors displayed on the sliders in the Color Editor dialog
  * Added option (on by default) to Bline Tool to allow position of regions and outlines to be automatically linked (#1776156)
  * Added menu entry to restore all dialogs to their default positions
  * Change the way the red frame indicating Animate Editing Mode is displayed, so it shows up in all themes (#1801220)
  * Allow user to "export SYNFIG_DISABLE_POPUP_WINDOWS=1" to make splash screen etc. less obtrusive
  * Fixed the logic used to decide which value to use when linking values together
  * Update child canvas time sliders when parent time slider is moved
  * Prevent the caret menu entries from being disabled when another canvas is closed
  * Show the correct initial quality level in the View > Preview Quality menu

 0.61.06 (SVN 543) - June 20, 2007 - Bug fixes
 
  * Fix amd64 issues
  * Fix corruption of gamma settings
  * Fix manipulating layers in the layer dialog
  * Disable the timebar instead of hiding it
  * Fix File -> Close & input devices close button
  * Fix targets that crash in synfigstudio
  * Fix some doxygen warnings
  * Fixes in the sketch tool
  * Fixes in the draw tool
  * Fix several crashes
  * Fix bline looping
  * Misc Win32 fixes
  * Add optional GNOME thumbnailer
  * Rework several dialogs to use GNOME HIG
  * Disable several tools, see FAQ to re-enable
  * Disable the empty Palette Browser dialog
  * Add mousewheel support in canvas windows
  * Add tooltips to preview window & give it a default size
  * Display HTML values in the colour dialog
  * Add an icon for the curves dialog
  * New Tango ArtLibreSet styled icons by PXEGeek

 0.61.05 (SVN 128) - February 27, 2006 - build issues

  * Fixed building with gtkmm 2.8 (#1373933, #1358304)
  * Allow the toolbox to come to the front (#1367916)
  * Make the user prefs dir configurable at build time
  * Fixed minor build issues

 0.61.04 (SVN 103) - January 10, 2006 - MacOS X packaging

  * Fixes for the MacOS X packaging

 0.61.03 - December 8, 2005 - Build & copyright fixes

  * Update more old copyright and licence notices
  * Fully fix building with GCC 4

 0.61.02 - November 26, 2005 - Build fixes

  * Better Win32 / MacOS X packaging
  * Use new GTK+ file chooser
  * Adds some GCC 4 tweaks
  * Fixes crash on layer reorder
  * Include errno.h where needed
  * Other tweaks

 0.61.01 - November 6, 2005 - Copyright updates

  * Update old copyright and licence notices
  * Add ./configure options for debug, warnings, optimisation, profiling
  * Uses per-os directory separator
  * Add TODO file

 0.61.00-39 - November 1, 2005 - Developer preview

  * First public release!

