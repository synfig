#! /bin/sh
#
# Synfig Studio Bootstrap Script
# $Id$
# 
# This script creates the configure script and Makefile.in files,
# and also fixes a few things in both to ensure a smooth build
# on all compilers and platforms.
#

# Grab the current directory and move to our own
CURR_DIR=$(pwd)
cd $(dirname $0)

# Environment Variables
BOOTSTRAP_NAME=$(basename $0)
CONFIG_DIR="config"

. "$CONFIG_DIR/build.cfg"

SED_SCRIPT="
s/@PACKAGE@/$PACKAGE/g;
s/@PACKAGE_NAME@/$PACKAGE_NAME/g;
s/@PACKAGE_BUGREPORT@/$PACKAGE_BUGREPORT/g;
s/@PACKAGE_TARNAME@/$PACKAGE_TARNAME/g;
s/@PACKAGE_VERSION@/$PACKAGE_VERSION/g;
s|@SVN_REPOSITORY@|$SVN_REPOSITORY|g;
s/@VERSION@/$VERSION/g;
s/@API_VERSION@/$API_VERSION/g;
s/@VERSION_MAJ@/$VERSION_MAJ/g;
s/@VERSION_MIN@/$VERSION_MIN/g;
s/@VERSION_REV@/$VERSION_REV/g;
s/@VERSION_REL@/$VERSION_REL/g;
s/@CFLAGS@//g;
"

# Required automake and autoconf versions
#AUTOCONF_VERSION=2.5
#AUTOMAKE_VERSION=1.6
#LIBTOOL_VERSION=1.4
export WANT_AUTOMAKE=1.8
export WANT_AUTOCONF_2_5=1

LIBTOOLIZE=libtoolize
INTLTOOLIZE=intltoolize
AUTOCONF=autoconf
AUTOMAKE=automake
ACLOCAL=aclocal
AUTOHEADER=autoheader

# Define the output function
output () {
	echo $BOOTSTRAP_NAME: $*
}

# Define the cleanup function
cleanup () {
	output Cleaning up...
	rm -fr config.cache autom4te*.cache configure.in $TEMPFILE
}

output Preparing build environment for $PACKAGE-$VERSION...

# Look for the CVS directory. If we don't find it, we need to
# ask the user if they know what they are doing.
( test -d CVS || test -d .svn ) ||
{
	echo "
$BOOTSTRAP_NAME: warning: This shell script is intended for those
$BOOTSTRAP_NAME: warning: who either know what they are doing or
$BOOTSTRAP_NAME: warning: or downloaded this program from the CVS
$BOOTSTRAP_NAME: warning: repository. See README for more details.
$BOOTSTRAP_NAME: warning: To avoid seeing this message in the future,
$BOOTSTRAP_NAME: warning: create an empty directory called 'CVS'."
	echo Waiting for 15 seconds...
	sleep 15
}

# Create the temporary file
output Creating temporary file...
TEMPFILE=`mktemp /tmp/$BOOTSTRAP_NAME.XXXXXX` ||
{
	output ERROR: Unable to create temporary file!
	exit 1
}

# Check for autoconf
(which autoconf > /dev/null 2>&1 ) ||
{
	output error: 'Could not find GNU autoconf!'
	output You need to download and install GNU autoconf v2.52 or higher.
	output '<ftp://ftp.gnu.org/gnu/autoconf>'
	cleanup;
	exit 1
}

# Check autoconf version
output Using $(autoconf --version | grep utoconf)
autoconf --version | grep -q "$AUTOCONF_VERSION" || echo \
"$BOOTSTRAP_NAME: warning: Unexpected version of GNU Autoconf (expected $AUTOCONF_VERSION)
$BOOTSTRAP_NAME: warning: *** Bootstrap process may fail!"

# Check for automake
(which automake > /dev/null 2>&1 ) ||
{
	output error: 'Could not find GNU automake!'
	output You need to download and install GNU automake v1.5 or higher.
	output '<ftp://ftp.gnu.org/gnu/automake>'
	cleanup;
	exit 1
}

# Check automake version
output Using $(automake --version | grep utomake)
automake --version | grep -q "$AUTOMAKE_VERSION" || echo \
"$BOOTSTRAP_NAME: warning: Unexpected version of GNU Automake (expected $AUTOMAKE_VERSION)
$BOOTSTRAP_NAME: warning: *** Bootstrap process may fail!"

# Check for libtool
(which libtool > /dev/null 2>&1 ) ||
{
	output error: 'Could not find GNU libtool!'
	output You need to download and install GNU libtool v1.4 or higher.
	output '<ftp://ftp.gnu.org/gnu/libtool>'
	exit 1
}

# Check libtool version
output Using $(libtoolize --version | grep ibtool)
libtoolize --version | grep -q "$LIBTOOL_VERSION" || echo \
"$BOOTSTRAP_NAME: warning: Unexpected version of GNU Libtool (expected $LIBTOOL_VERSION)
$BOOTSTRAP_NAME: warning: *** Bootstrap process may fail!"


aclocalfix () {
sed '
s:${srcdir}/intltool:${ac_aux_dir}/intltool:g;
s.printf ("\\t%s @ %ld ;\\n", erva+name_rva, 1+ i);.printf ("\\t\\"%s\\" @ %ld ;\\n", erva+name_rva, 1+ i);.;
' < aclocal.m4 > $TEMPFILE &&
rm aclocal.m4 &&
mv $TEMPFILE aclocal.m4
}

for FILENAME in project.spec ; do {
output Creating $FILENAME...
sed "$SED_SCRIPT" < "$CONFIG_DIR/$FILENAME.in" > "$FILENAME";
} ; done

output Renaming project.spec to $PACKAGE.spec...
mv project.spec "$PACKAGE.spec"

output Finishing up $PACKAGE.spec...
echo %changelog >> "$PACKAGE.spec"
cat NEWS >> "$PACKAGE.spec"

output Creating configure.in from configure.ac...
sed "$SED_SCRIPT" < "$CONFIG_DIR/configure.ac" > configure.in;

output Generating ChangeLog from SVN
if test x != "x$VERSION_REL" ; then export REVISION="--revision $VERSION_REL" ; fi
test -f ChangeLog || svn2cl --include-rev $REVISION $SVN_REPOSITORY/trunk/ || touch ChangeLog

output Setting up build environment...

# Set the shell to output what we are doing
set -x

# Create all of the build environment files
(
#	$INTLTOOLIZE -c &&
	$LIBTOOLIZE -c -f &&
	$ACLOCAL -I "$CONFIG_DIR" $ACLOCAL_FLAGS &&
	aclocalfix &&
	$AUTOHEADER &&
	$AUTOCONF -o configure &&
	$AUTOMAKE --force-missing --add-missing --include-deps &&
	true
) ||
{
	# Something went wrong...
	set +x
	output Failure.
	cleanup;
	exit 1
}

# Turn off echoing of commands
set +x

#output Patching configure script to look for gcc3...
#sed "
#s/g++ c++/g++3 g++ c++/;
#s/gcc cc/gcc3 gcc cc/;
#s:"'${prefix}/include'":"'${prefix}/include/synfig'":;
#s:PREFIX/include:PREFIX/include/ETL:;
#" < configure > $TEMPFILE
#cp $TEMPFILE configure

# Patch the Makefile.in files
for filename in $(find Makefile.in src -name Makefile.in) ; do {
	echo $BOOTSTRAP_NAME: Patching $filename
	(
		cp $filename $TEMPFILE &&
		sed "
			s;-I. ;;
			s;-I"'$(srcdir)'" ;-I"'$(top_srcdir)'" ;
			s;configure.in;config/configure.ac;
		" < $TEMPFILE > $filename
	) ||
	{
		# Patch failure
		output Failure. Unable to patch $filename.
		cleanup;
		exit 1
	}
}; done

output Creating Makefile...
( echo "
all:
	./configure --enable-maintainer-mode
	make all
	
install:
	./configure --enable-maintainer-mode
	make install
	
check:
	./configure --enable-maintainer-mode
	make check

distcheck:
	./configure --enable-maintainer-mode
	make check
	
dist:
	./configure --enable-maintainer-mode
	make dist

TAG=${PACKAGE_TARNAME}_${VERSION_MAJ}_${VERSION_MIN}_${VERSION_REV}

tagstable:
	-svn delete $SVN_REPOSITORY/tags/stable -m \"Stabe Tag: Removing old tag\"
	svn mkdir $SVN_REPOSITORY/tags/stable -m \"Stable Tag: Creating new directory\"
	svn copy . $SVN_REPOSITORY/tags/stable -m \"Stable Tag: Copying everyhting over\"

tagrelease:
	svn mkdir $SVN_REPOSITORY/tags/\$(TAG) -m \"Release Tag: Making directory\"
	svn copy . $SVN_REPOSITORY/tags/\$(TAG) -m \"Release \$(TAG)\"

" ) > Makefile

echo $BOOTSTRAP_NAME: Complete.

cleanup;

# Move back to the current directory
cd "$CURR_DIR"
